#include <windows.h>

#ifndef EVENT_CHILD_EVENTCHILD_H_H
#define EVENT_CHILD_EVENTCHILD_H_H

DWORD WINAPI ChildThread(LPVOID lpParam);

typedef struct MyData {
    HANDLE* lpParentEvent;
    int count;
} MYDATA, *PMYDATA;

#endif //EVENT_CHILD_EVENTCHILD_H_H
