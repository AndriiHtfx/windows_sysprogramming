#include <iostream>
#include <windows.h>
#include "EventChild.h"

#define MAX_THREADS 10

int main() {
    HANDLE labPipe = CreateFile(
            TEXT("\\\\.\\pipe\\lab_pipe"),
            GENERIC_READ,
            FILE_SHARE_READ | FILE_SHARE_WRITE,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL
    );

    if (labPipe == INVALID_HANDLE_VALUE) {
        std::cerr << "error: failed to connect to pipe: error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    BOOL result = FALSE;
    DWORD dwBytesRead;

    HANDLE parentEvent;
    result = ReadFile(
            labPipe,
            &parentEvent,
            sizeof(HANDLE),
            &dwBytesRead,
            NULL
    );

    if (!result) {
        std::cerr << "error, failed to read from pipe, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    HANDLE hThreadArray[MAX_THREADS];
    PMYDATA pDataArray[MAX_THREADS];
    DWORD dwThreadIdArray[MAX_THREADS];

    for (int i=0; i<MAX_THREADS; i++) {
        pDataArray[i] = (PMYDATA) HeapAlloc(
                GetProcessHeap(),
                HEAP_ZERO_MEMORY,
                sizeof(MYDATA)
        );

        if (pDataArray[i] == NULL) {
            std::cerr << "error: failed to heap memory, error code: " << GetLastError() << std::endl;
            return (int) GetLastError();
        }

        pDataArray[i]->count = i;
        pDataArray[i]->lpParentEvent = &parentEvent;
        hThreadArray[i] = CreateThread(
                NULL,
                0,
                ChildThread,
                pDataArray[i],
                CREATE_SUSPENDED, 
                &dwThreadIdArray[i]
        );

        if (hThreadArray[i] == NULL) {
            std::cerr << "error: failed to create thread, error code: " << GetLastError() << std::endl;
            return (int) GetLastError();
        }
    }

    for (auto thread: hThreadArray) {
        ResumeThread(thread);
        WaitForSingleObject(thread, INFINITE);
    }

    WaitForMultipleObjects(MAX_THREADS, hThreadArray, TRUE, INFINITE);

    for (auto thread: hThreadArray) {
        CloseHandle(thread);
    }

    CloseHandle(parentEvent);

    return 0;
}
