#include <windows.h>
#include "EventChild.h"
#include <iostream>
#include <time.h>
#include <stdlib.h>

DWORD WINAPI ChildThread(LPVOID lpParam)
{
    PMYDATA pData = (PMYDATA) lpParam;
    HANDLE* parentEvent = pData->lpParentEvent;
    int threadNumber = pData->count;

    ResetEvent(*parentEvent);

    srand(time(NULL));
    int sleepTime = rand() % 10 + 1;

    std::cout << "I'm process #" << threadNumber << " and I gonna sleep for " << sleepTime << " seconds" <<  std::endl;
    Sleep(sleepTime * 1000);
    SetEvent(*parentEvent);

    return 0;
}
