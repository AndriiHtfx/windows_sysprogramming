#include <iostream>
#include <windows.h>

#define  EXECUTABLE_PATH "C:\\Users\\dehty\\CLionProjects\\Event_Child\\cmake-build-debug\\Event_Child.exe"

int main() {
    SECURITY_ATTRIBUTES eventSA;
    eventSA.nLength = sizeof(eventSA);
    eventSA.lpSecurityDescriptor = NULL;
    eventSA.bInheritHandle = TRUE; // вот)

    HANDLE event = CreateEvent(
            &eventSA,
            TRUE,
            FALSE,
            NULL
    );

    if (GetLastError() == ERROR_ALREADY_EXISTS) {
        std::cerr << "warning: event object already exists!" << std::endl;
    }

    HANDLE labPipe = CreateNamedPipe(
            TEXT("\\\\.\\pipe\\lab_pipe"),
            PIPE_ACCESS_OUTBOUND,
            PIPE_TYPE_BYTE,
            1,
            0,
            0,
            0,
            NULL
    );

    if (labPipe == INVALID_HANDLE_VALUE) {
        std::cerr << "error: failed to create named pipe, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }


    STARTUPINFO info={sizeof(info)};
    PROCESS_INFORMATION processInfo;

    CreateProcess(
            EXECUTABLE_PATH,
            NULL,
            NULL,
            NULL,
            TRUE, // разрешаем наследование описателей родительского процесса
            NULL,
            NULL,
            NULL,
            &info,
            &processInfo
    );

    BOOL result = FALSE;

    result = ConnectNamedPipe(labPipe, NULL);

    if (!result) {
        std::cerr << "failed to connect to named pipe, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    DWORD dwBytesWritten;
    result = WriteFile(
            labPipe,
            &event,
            sizeof(event),
            &dwBytesWritten,
            NULL
    );

    if (!result) {
        std::cerr << "error: failed to send data, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    WaitForSingleObject(processInfo.hProcess, INFINITE);

    return 0;
}
