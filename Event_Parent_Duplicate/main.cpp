#include <iostream>
#include <windows.h>

#define  EXECUTABLE_PATH "C:\\Users\\dehty\\CLionProjects\\Event_Child\\cmake-build-debug\\Event_Child.exe"

int main() {
    HANDLE event = CreateEvent(
            NULL,
            TRUE,
            FALSE,
            NULL
    );

    if (GetLastError() == ERROR_ALREADY_EXISTS) {
        std::cerr << "warning: event object already exists!" << std::endl;
    }

    HANDLE labPipe = CreateNamedPipe(
            TEXT("\\\\.\\pipe\\lab_pipe"),
            PIPE_ACCESS_OUTBOUND,
            PIPE_TYPE_BYTE,
            1,
            0,
            0,
            0,
            NULL
    );

    if (labPipe == INVALID_HANDLE_VALUE) {
        std::cerr << "error: failed to create named pipe, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }


    STARTUPINFO info={sizeof(info)};
    PROCESS_INFORMATION processInfo;

    CreateProcess(
            EXECUTABLE_PATH,
            NULL,
            NULL,
            NULL,
            FALSE,
            NULL,
            NULL,
            NULL,
            &info,
            &processInfo
    );

    BOOL result = FALSE;

    result = ConnectNamedPipe(labPipe, NULL);

    if (!result) {
        std::cerr << "failed to connect to named pipe, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    HANDLE newEvent;

    result = DuplicateHandle(
            GetCurrentProcess(),   // исходный процесс
            event,                 // исходный объект
            processInfo.hProcess,  // целевой процесс
            &newEvent,             // целевой объект. !передается по ссылке!
            DUPLICATE_SAME_ACCESS, // права доступа
            FALSE,                 // права наследования дочерними процессами дочернего процесса
            NULL                   // опции (DUPLICATE_SAME_ACCESS / DUPLICATE_CLOSE_SOURCE)
    );

    if (!result) {
        std::cerr << "error: failed to duplicate file mapping, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    DWORD dwBytesWritten;
    result = WriteFile(
            labPipe,
            &newEvent,
            sizeof(event),
            &dwBytesWritten,
            NULL
    );

    if (!result) {
        std::cerr << "error: failed to send data, error code: " << GetLastError() << std::endl;
        return (int) GetLastError();
    }

    WaitForSingleObject(processInfo.hProcess, INFINITE);

    return 0;
}
